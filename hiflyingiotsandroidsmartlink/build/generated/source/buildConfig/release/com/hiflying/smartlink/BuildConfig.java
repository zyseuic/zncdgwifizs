/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.hiflying.smartlink;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "com.hiflying.smartlink";
  /**
   * @deprecated APPLICATION_ID is misleading in libraries. For the library package name use LIBRARY_PACKAGE_NAME
   */
  @Deprecated
  public static final String APPLICATION_ID = "com.hiflying.smartlink";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 89;
  public static final String VERSION_NAME = "8.3.1";
}
