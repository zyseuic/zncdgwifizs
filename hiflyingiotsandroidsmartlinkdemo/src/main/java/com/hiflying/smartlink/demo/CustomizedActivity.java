package com.hiflying.smartlink.demo;

import com.hiflying.smartlink.demo.R;
import com.hiflying.smartlink.ISmartLinker;
import com.hiflying.smartlink.OnSmartLinkListener;
import com.hiflying.smartlink.SmartLinkedModule;
import com.hiflying.smartlink.v3.SnifferSmartLinker;
import com.hiflying.smartlink.v7.MulticastSmartLinker;

import android.Manifest;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CustomizedActivity extends Activity implements OnSmartLinkListener{
	
	public static final String EXTRA_SMARTLINK_VERSION = "EXTRA_SMARTLINK_VERSION";
	
	private static final String TAG = "CustomizedActivity";

	protected EditText mSsidEditText;
	protected EditText mPasswordEditText;
	protected EditText mOthersEditText;
	protected Button mStartButton;
	private TextView tv_Result;
	private View mMixedTypeRow;
	private Spinner mMixedTypeSpinner;
	protected ISmartLinker mSmartLinker;
	private boolean mIsConncting = false;
	protected Handler mViewHandler = new Handler();
	protected ProgressDialog mWaitingDialog;
	private BroadcastReceiver mWifiChangedReceiver;
	 private  Thread thread;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		int smartLinkVersion = getIntent().getIntExtra(EXTRA_SMARTLINK_VERSION, 3);
//		if(smartLinkVersion == 7) {
		////			mSmartLinker = MulticastSmartLinker.getInstance();
		////		}else {
		////			mSmartLinker = SnifferSmartLinker.getInstance();
		////		}
		mSmartLinker = MulticastSmartLinker.getInstance();
		mWaitingDialog = new ProgressDialog(this);
		mWaitingDialog.setMessage(getString(R.string.hiflying_smartlinker_waiting));
		mWaitingDialog.setButton(ProgressDialog.BUTTON_NEGATIVE, getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		mWaitingDialog.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss(DialogInterface dialog) {

				mSmartLinker.setOnSmartLinkListener(null);
				mSmartLinker.stop();
				mIsConncting = false;
			}
		});
		
		setContentView(R.layout.activity_customized);
		tv_Result=(TextView)findViewById(R.id.tv_Result);
		mSsidEditText = (EditText) findViewById(R.id.editText_hiflying_smartlinker_ssid);
		//开启权限
		checkLocationEnable();
		mPasswordEditText = (EditText) findViewById(R.id.editText_hiflying_smartlinker_password);
		mOthersEditText = (EditText) findViewById(R.id.editText_hiflying_smartlinker_others);
		mMixedTypeRow = findViewById(R.id.tableRow_hiflying_smartlinker_mixed);
		mMixedTypeSpinner = (Spinner) findViewById(R.id.spinner_hiflying_smartlinker_mixed);
		mStartButton = (Button) findViewById(R.id.button_hiflying_smartlinker_start);
		mSsidEditText.setText(getSSid());
		if (smartLinkVersion == 7) {
			//2019年11月18日14:07:18 ：周宇：不显示mMixedTypeRow控件
			//并且 other的控件也不显示
			//mMixedTypeRow.setVisibility(View.VISIBLE);
			mMixedTypeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
					int mixType = 0;
					if (position == 0) {
						mixType = MulticastSmartLinker.MIX_TYPE_SMART_LINK_V8;
					}else if (position == 1) {
						mixType = MulticastSmartLinker.MIX_TYPE_SMART_LINK_V3;
					}
					((MulticastSmartLinker)mSmartLinker).setMixType(mixType);
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		}

		mStartButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!mIsConncting){
					//设置要配置的ssid 和pswd
					try {
						mSmartLinker.setOnSmartLinkListener(CustomizedActivity.this);
						mSmartLinker.setOthers(mOthersEditText.getText().toString().trim());
						//开始 smartLink
						mSmartLinker.start(getApplicationContext(), mPasswordEditText.getText().toString().trim(), 
								mSsidEditText.getText().toString().trim());
						mIsConncting = true;
						mWaitingDialog.show();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		
		mWifiChangedReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
				if (networkInfo != null && networkInfo.isConnected()) {
					mSsidEditText.setText(getSSid());
					mPasswordEditText.requestFocus();
					mStartButton.setEnabled(true);
				}else {
					mSsidEditText.setText(getString(R.string.hiflying_smartlinker_no_wifi_connectivity));
					mSsidEditText.requestFocus();
					mStartButton.setEnabled(false);
					if (mWaitingDialog.isShowing()) {
						mWaitingDialog.dismiss();
					}
				}
			}
		};
		registerReceiver(mWifiChangedReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
		handler.postDelayed(runnable, TIME);
	}
	private int TIME = 1;//每隔10ms执行一次.
	Handler handler = new Handler();
	Runnable runnable = new Runnable() {
		public void run() {
			try {
				handler.postDelayed(this, TIME);
				Log.i("zy","11"+mSsidEditText.getText());
				if(mSsidEditText.getText().toString().contains("<unknown ssid>")) {
					Log.i("zy","12"+mSsidEditText.getText());
					mSsidEditText.setText(getSSid());
				}
				else{
					stop();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};
	public void stop() {
		handler.removeCallbacks(runnable);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		mSmartLinker.setOnSmartLinkListener(null);
		try {
			unregisterReceiver(mWifiChangedReceiver);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public void onLinked(final SmartLinkedModule module) {
		// TODO Auto-generated method stub
		
		Log.w(TAG, "onLinked");
		mViewHandler.post(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), getString(R.string.hiflying_smartlinker_new_module_found, module.getId(), module.getMac(), module.getModuleIP()),
						Toast.LENGTH_SHORT).show();
			}
		});
	}


	@Override
	public void onCompleted() {
		
		Log.w(TAG, "onCompleted");
		mViewHandler.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), getString(R.string.hiflying_smartlinker_completed), 
						Toast.LENGTH_SHORT).show();
				//tv_Result.setText("SmartLink 完成");
				mWaitingDialog.dismiss();
				mIsConncting = false;
			}
		});
	}


	@Override
	public void onTimeOut() {
		
		Log.w(TAG, "onTimeOut");
		mViewHandler.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), getString(R.string.hiflying_smartlinker_timeout), 
						Toast.LENGTH_SHORT).show();
				mWaitingDialog.dismiss();
				mIsConncting = false;
			}
		});
	}	

	private String getSSid(){
		//WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
		WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
		if(wm != null){
			WifiInfo wi = wm.getConnectionInfo();
			if(wi != null){
				String ssid = wi.getSSID();
				if(ssid.length()>2 && ssid.startsWith("\"") && ssid.endsWith("\"")){
					return ssid.substring(1,ssid.length()-1);
				}else{
					return ssid;
				}
			}
		}

		return "";
	}


	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == event.KEYCODE_BACK || keyCode == event.KEYCODE_HOME) {
			finishAlert();
		}
		return super.onKeyDown(keyCode, event);
	}
	//退出系统提示框
	public void finishAlert() {
		new AlertDialog.Builder(this)
				.setTitle("系统提示")
				// 设置对话框标题
				.setMessage("是否要退出系统？")
				// 设置显示的内容
				//右边按钮
				.setPositiveButton("确定",
						new DialogInterface.OnClickListener() {// 添加确定按钮
							@Override
							public void onClick(DialogInterface dialog,
												int which) {// 确定按钮的响应事件

								//loginOut(false);
								System.exit(0);
							}
						})
				//左边按钮
				.setNegativeButton("取消", null).show();// 在按键响应事件中显示此对话框
	}
	//------------------获取定位权限--------------------------------
	private static final int ACCESS_FINE_LOCATION_PERMISSION_REQUEST = 100;
	private static final int REQUEST_ACTION_LOCATION_SETTINGS = 3;

	private void checkLocationEnable() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
				requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_PERMISSION_REQUEST);
			}
		}
		if (!isLocationEnabled()) {
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivityForResult(intent, REQUEST_ACTION_LOCATION_SETTINGS);
		}
	}

	private boolean isLocationEnabled() {
		int locationMode = 0;
		String locationProviders;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			try {
				locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
			} catch (Settings.SettingNotFoundException e) {
				e.printStackTrace();
				return false;
			}
			return locationMode != Settings.Secure.LOCATION_MODE_OFF;
		} else {
			locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
			return !TextUtils.isEmpty(locationProviders);
		}
	}
}
