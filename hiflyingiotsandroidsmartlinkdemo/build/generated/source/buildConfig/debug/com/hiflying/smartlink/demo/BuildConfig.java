/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.hiflying.smartlink.demo;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.hiflying.smartlink.demo";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 89;
  public static final String VERSION_NAME = "8.3.1";
}
